## Statistics Service

| REST Endpoint               | Type                           | Description   |
|-----------------------------|--------------------------------|--------------|
| `/statistics/topNTracks`    | GET                            | Get top N tracks by their aggregated amounts in descending order. `limit` to select N which by default is set to 5.|

### Run the service
You require JDK 11 and Maven to run this service.

Run it:
```
mvn clean spring-boot:run
```

Now you can access top tracks using the following url:
```
http://localhost:8080/statistics/topNTracks
```
This will return you top five tracks by aggregated amounts and in EUR currency. You can add query parameter `limit` for number of tracks.

Run all tests:
```
mvn clean test
```

### Implementation details
I ended up deciding to use Spring Batch for loading DSP Streaming Reports into an in-memory H2 database at initial loading of the application. It would be much easier to parse those CSVs with some third party library with a one line command. However, I thought that in real life, you would get these files from DSPs that are massive and should be uploaded in batches and can be done nicely with Spring Batch and Apache Kafka integration to leverage a distributed architecture.

Rest of the application is built using Spring Boot with a controller, service, and repository each. I used Lombok to reduce boilerplate code. Integration tests are currently also using Spring Batch to load some sample test dataset when running the tests. This is not the right way because if tests manipulate that data and persist it, other tests would not work correctly. I could either use fixtures here or dockerize the tests. 

There is a `exchange_rates` table which tracks currency conversions on a daily basis. For the sake of demonstration, I added some data points manually.

Initially I developed a solution where I aggregated tracks by amounts on each call to the endpoint (commit: 55a2f8570d22e31ad80702bbe76155d22495ffb0). Eventually I decided to process aggregates at the time of running batch imports so the result is pre-computed for this REST endpoint.

### How could we make this a production grade service?
* Dockerize this application. Use docker-compose for this service itself and another service for a SQL database here.
* Expanding on dockerizing topic, I'd then leverage that setup for integration tests; I would basically use `testcontainers` to run integration tests in isolated containers so if one of the tests modifies some data and persists it, it wouldn't affect other tests.
* API Versioning and HATEOAS.
* Error logging using a library such as `SLF4J`.
* Move Spring Batch metadata tables into a dedicated schema. Right now it ends up in the same schema as this application's which looks cluttered.
* Add API documentation using a tool such as Swagger.
* Encryption of database connection credentials.