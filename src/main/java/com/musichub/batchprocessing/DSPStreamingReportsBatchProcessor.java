package com.musichub.batchprocessing;

import com.musichub.models.ReportRow;
import com.musichub.models.TopTrack;
import java.time.LocalDate;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.sql.DataSource;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.database.JpaPagingItemReader;
import org.springframework.batch.item.database.builder.JdbcBatchItemWriterBuilder;
import org.springframework.batch.item.database.builder.JpaPagingItemReaderBuilder;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.MultiResourceItemReader;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.batch.item.file.builder.MultiResourceItemReaderBuilder;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;

@Configuration
@EnableBatchProcessing
public class DSPStreamingReportsBatchProcessor {

  private static final String DELIMITER = ";";
  private static final String[] FIELD_NAMES = {"ISRC", "TRACK_NAME", "ARTIST_NAME", "UNITS",
      "AMOUNT", "CURRENCY"};
  private static final String INSERT_REPORT_ROW_SQL =
      "INSERT INTO dsp_streaming_reports_data "
          + "(isrc, track_name, artist_name, units, amount, currency, created_at) "
          + "VALUES (:isrc, :trackName, :artistName, :units, :amount, :currency, :createdAt)";
  private static final String INSERT_TOP_TRACKS_AGGREGATED_IN_EUR =
      "INSERT INTO top_tracks (artist_name, isrc, track_name, amount)\n"
          + "VALUES (:artistName, :isrc, :trackName, :amount)";
  private static final String SELECT_TOP_TRACKS_AGGREGATED_IN_EUR =
      "SELECT new com.musichub.models.TopTrack(report.artistName, "
          + "report.isrc, report.trackName, sum(report.amount*rate.exchangeRate) AS amount)\n"
          + "FROM ReportRow report\n"
          + "JOIN CurrencyExchangeRate rate\n"
          + "ON rate.currencyExchangeRateId.fromCurrency = report.currency\n"
          + "WHERE rate.currencyExchangeRateId.toCurrency = 'EUR'\n"
          + "GROUP BY report.artistName, report.isrc, report.trackName\n"
          + "ORDER BY amount DESC, report.trackName";

  @Autowired
  public JobBuilderFactory jobBuilderFactory;

  @Autowired
  public StepBuilderFactory stepBuilderFactory;

  @PersistenceContext
  private EntityManager em;

  @Value("${REPORT_FILES}")
  private Resource[] inputFiles;

  public MultiResourceItemReader<ReportRow> multiResourceItemReader() {
    return new MultiResourceItemReaderBuilder<ReportRow>()
        .name("dspStreamingReportMultiResourceItemReader")
        .resources(inputFiles)
        .delegate(reader())
        .build();
  }

  public FlatFileItemReader<ReportRow> reader() {
    return new FlatFileItemReaderBuilder<ReportRow>()
        .name("dspStreamingReportFlatFileItemReader")
        .linesToSkip(1)
        .lineMapper(defaultLineMapper())
        .build();
  }

  public JpaPagingItemReader<TopTrack> topTracksReader() {
    return new JpaPagingItemReaderBuilder<TopTrack>()
        .name("aggregateTopTracksByAmount")
        .queryString(SELECT_TOP_TRACKS_AGGREGATED_IN_EUR)
        .entityManagerFactory(em.getEntityManagerFactory())
        .pageSize(10)
        .build();
  }

  @Bean
  public JdbcBatchItemWriter<ReportRow> dspStreamingReportsWriter(DataSource dataSource) {
    return new JdbcBatchItemWriterBuilder<ReportRow>()
        .itemSqlParameterSourceProvider(new BeanPropertyItemSqlParameterSourceProvider<>())
        .sql(INSERT_REPORT_ROW_SQL)
        .dataSource(dataSource)
        .build();
  }

  @Bean
  public JdbcBatchItemWriter<TopTrack> topTracksWriter(DataSource dataSource) {
    return new JdbcBatchItemWriterBuilder<TopTrack>()
        .itemSqlParameterSourceProvider(new BeanPropertyItemSqlParameterSourceProvider<>())
        .sql(INSERT_TOP_TRACKS_AGGREGATED_IN_EUR)
        .dataSource(dataSource)
        .build();
  }

  @Bean
  public Step step1(JdbcBatchItemWriter<ReportRow> dspStreamingReportsWriter) {
    return stepBuilderFactory.get("step1")
        .<ReportRow, ReportRow>chunk(10)
        .reader(multiResourceItemReader())
        .processor((ItemProcessor<ReportRow, ReportRow>) item -> {
          item.setCreatedAt(LocalDate.now());
          return item;
        })
        .writer(dspStreamingReportsWriter)
        .build();
  }

  @Bean
  public Step step2(JdbcBatchItemWriter<TopTrack> topTracksWriter) {
    return stepBuilderFactory.get("step2")
        .<TopTrack, TopTrack>chunk(10)
        .reader(topTracksReader())
        .writer(topTracksWriter)
        .build();
  }

  @Bean
  public Job importJob(Step step1, Step step2) {
    return jobBuilderFactory.get("importJob")
        .start(step1)
        .next(step2)
        .build();
  }

  private DefaultLineMapper<ReportRow> defaultLineMapper() {
    DelimitedLineTokenizer tokenizer = new DelimitedLineTokenizer(DELIMITER);
    tokenizer.setNames(FIELD_NAMES);
    DefaultLineMapper<ReportRow> customerLineMapper = new DefaultLineMapper<>();
    customerLineMapper.setLineTokenizer(tokenizer);
    customerLineMapper.setFieldSetMapper(new BeanWrapperFieldSetMapper<>() {{
      setTargetType(ReportRow.class);
    }});
    customerLineMapper.afterPropertiesSet();
    return customerLineMapper;
  }
}
