package com.musichub.controllers;

import static java.util.stream.Collectors.toList;

import com.musichub.dtos.TopTrackDTO;
import com.musichub.services.StatisticsService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/statistics")
public class StatisticsController {

  @Autowired
  private StatisticsService statisticsService;

  @GetMapping("/topNTracks")
  @ResponseBody
  public List<TopTrackDTO> topNTracks(
      @RequestParam(name = "limit", defaultValue = "5") Integer limit) {
    return statisticsService.topNTracksByRevenue(limit).stream()
        .map(TopTrackDTO::of).collect(toList());
  }
}
