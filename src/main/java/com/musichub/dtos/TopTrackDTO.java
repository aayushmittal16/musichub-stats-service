package com.musichub.dtos;


import com.musichub.models.TopTrack;
import java.math.BigDecimal;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class TopTrackDTO {

  private String trackName;
  private String isrc;
  private String artistName;
  private BigDecimal amount;

  private TopTrackDTO() {
  }

  public static TopTrackDTO of(TopTrack track) {
    return new TopTrackDTO(track.getTrackName(), track.getIsrc(), track.getArtistName(),
        track.getAmount());
  }
}
