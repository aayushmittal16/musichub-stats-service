package com.musichub.models;

import java.math.BigDecimal;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Entity
@Table(name = "exchange_rates")
public class CurrencyExchangeRate {

  @EmbeddedId
  private CurrencyExchangeRateId currencyExchangeRateId;
  private BigDecimal exchangeRate;
}
