package com.musichub.models;

import java.io.Serializable;
import java.time.LocalDate;
import javax.persistence.Embeddable;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Embeddable
public class CurrencyExchangeRateId implements Serializable {

  private LocalDate exchangeDate;
  private String fromCurrency;
  private String toCurrency;
}
