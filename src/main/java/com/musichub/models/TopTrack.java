package com.musichub.models;

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "top_tracks")
public class TopTrack {

  private String trackName;
  @Id
  private String isrc;
  private String artistName;
  @Column(precision = 11, scale = 5)
  private BigDecimal amount;
}
