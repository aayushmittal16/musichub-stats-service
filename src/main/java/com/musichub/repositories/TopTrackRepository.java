package com.musichub.repositories;

import com.musichub.models.TopTrack;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TopTrackRepository extends JpaRepository<TopTrack, String> {

}
