package com.musichub.services;

import com.musichub.models.TopTrack;
import com.musichub.repositories.TopTrackRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
public class StatisticsService {

  @Autowired
  private TopTrackRepository topTrackRepository;

  public Page<TopTrack> topNTracksByRevenue(int limit) {
    return topTrackRepository.findAll(PageRequest.of(0, limit, Sort.by("amount").descending()));
  }
}
