INSERT INTO exchange_rates (exchange_date, from_currency, to_currency, exchange_rate) VALUES
(CURRENT_TIMESTAMP(), 'USD', 'EUR', 0.84),
(CURRENT_TIMESTAMP(), 'GBP', 'EUR', 1.12);