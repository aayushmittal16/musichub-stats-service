package com.musichub;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import com.musichub.dtos.TopTrackDTO;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class StatisticsIntegrationTest {

  @LocalServerPort
  private int port;

  private String baseUrl;
  private final TestRestTemplate restTemplate = new TestRestTemplate();

  @Before
  public void setup() {
    baseUrl = String.format("http://localhost:%d", port);
  }

  @Test
  public void When_Request_TopNTracks_Then_ReturnNTracks() {
    int limit = 10;
    ResponseEntity<TopTrackDTO[]> responseEntity = restTemplate
        .getForEntity(baseUrl + "/statistics/topNTracks?limit={limit}", TopTrackDTO[].class, limit);
    assertEquals(200, responseEntity.getStatusCodeValue());
    TopTrackDTO[] topTracks = responseEntity.getBody();
    assertNotNull(topTracks);
    assertEquals(10, topTracks.length);
    boolean isSortedByAmount = true;
    for (int i = 1; i < topTracks.length; i++) {
      if (topTracks[i - 1].getAmount().compareTo(topTracks[i].getAmount()) < 0) {
        isSortedByAmount = false;
        break;
      }
    }
    assertTrue(isSortedByAmount);
  }

  @Test
  public void When_Request_TopNTracks_WithBadParameters_Then_Return_400Response() {
    ResponseEntity<String> responseEntity = restTemplate
        .getForEntity(baseUrl + "/statistics/topNTracks?limit=abcd", String.class);
    assertEquals(400, responseEntity.getStatusCodeValue());
  }
}
