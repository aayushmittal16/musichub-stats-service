package com.musichub.controllers;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.musichub.models.TopTrack;
import com.musichub.services.StatisticsService;
import java.math.BigDecimal;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageImpl;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@WebMvcTest(StatisticsController.class)
public class StatisticsControllerTest {

  private static final ObjectMapper objectMapper = new ObjectMapper();

  @Autowired
  private MockMvc mockMvc;
  @MockBean
  private StatisticsService statisticsService;

  private List<TopTrack> topTracks;

  @Before
  public void setup() {
    topTracks = List
        .of(new TopTrack("Thriller", "65037E030309", "The Slits", BigDecimal.valueOf(0.32)),
            new TopTrack("Backstreet's Back", "Z349S33869V0", "George Michael",
                BigDecimal.valueOf(0.50)));
    when(statisticsService.topNTracksByRevenue(5)).thenReturn(new PageImpl<>(topTracks));
  }

  @Test
  public void when_topNTracksByRevenue_Then_Return_200Response_With_Body() throws Exception {
    mockMvc.perform(get("/statistics/topNTracks"))
        .andExpect(status().isOk())
        .andExpect(content().string(objToJsonStr(topTracks)));
  }

  @Test
  public void when_topNTracksByRevenue_WithBadData_Then_Return_400Response() throws Exception {
    mockMvc.perform(get("/statistics/topNTracks?limit=abc"))
        .andExpect(status().isBadRequest());
  }

  private static String objToJsonStr(Object object) throws Exception {
    return objectMapper.writeValueAsString(object);
  }
}
