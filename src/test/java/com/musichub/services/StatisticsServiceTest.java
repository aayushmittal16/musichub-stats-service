package com.musichub.services;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import com.musichub.models.TopTrack;
import com.musichub.repositories.TopTrackRepository;
import java.math.BigDecimal;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class StatisticsServiceTest {

  private List<TopTrack> topTracks;

  @InjectMocks
  private StatisticsService statisticsService;

  @Mock
  private TopTrackRepository topTrackRepository;

  @Before
  public void setup() {
    topTracks = List
        .of(new TopTrack("Thriller", "65037E030309", "The Slits", BigDecimal.valueOf(0.32)),
            new TopTrack("Backstreet's Back", "Z349S33869V0", "George Michael",
                BigDecimal.valueOf(0.50)));
  }

  @Test
  public void When_topNTracksByRevenue_thenReturn_ListOfTopNTracks() {
    PageRequest pageRequest = PageRequest.of(0, 2, Sort.by("amount").descending());
    when(topTrackRepository.findAll(pageRequest))
        .thenReturn(new PageImpl<>(topTracks));
    List<TopTrack> actual = statisticsService.topNTracksByRevenue(2).toList();
    assertEquals(topTracks, actual);
    assertEquals(2, actual.size());

    pageRequest = PageRequest.of(0, 1, Sort.by("amount").descending());
    when(topTrackRepository.findAll(pageRequest))
        .thenReturn(new PageImpl<>(topTracks.subList(0, 1)));
    actual = statisticsService.topNTracksByRevenue(1).toList();
    assertEquals(topTracks.subList(0, 1), actual);
    assertEquals(1, actual.size());
  }
}
